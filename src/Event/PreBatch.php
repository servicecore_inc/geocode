<?php

namespace ServiceCore\Geocode\Event;

use Laminas\EventManager\Event;

class PreBatch extends Event
{
    public function __construct(int $startId)
    {
        parent::__construct(self::class, $startId);
    }
}
