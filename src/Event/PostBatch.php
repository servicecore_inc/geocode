<?php

namespace ServiceCore\Geocode\Event;

use Laminas\EventManager\Event;

class PostBatch extends Event
{
    public function __construct(array $batchedItems)
    {
        parent::__construct(self::class, $batchedItems);
    }
}
