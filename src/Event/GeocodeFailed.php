<?php

namespace ServiceCore\Geocode\Event;

use Laminas\EventManager\Event;
use ServiceCore\Geocode\RoleData\Geocodable;
use Throwable;

class GeocodeFailed extends Event
{
    /**
     * @param Geocodable $geocodable
     * @param Throwable  $throwable
     */
    public function __construct(Geocodable $geocodable, Throwable $throwable)
    {
        parent::__construct(self::class, $geocodable, ['throwable' => $throwable]);
    }

    /**
     * @return Throwable
     */
    public function getThrowable(): Throwable
    {
        return $this->getParam('throwable');
    }
}
