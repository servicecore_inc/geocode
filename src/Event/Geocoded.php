<?php

namespace ServiceCore\Geocode\Event;

use Laminas\EventManager\Event;
use ServiceCore\Geocode\RoleData\Geocodable;

class Geocoded extends Event
{
    public function __construct($target, Geocodable $entity)
    {
        parent::__construct(self::class, $target, ['geocodable' => $entity]);
    }

    public function getGeocodable()
    {
        return $this->getParam('geocodable');
    }

    public function setGeocodable(Geocodable $entity): self
    {
        $this->setParam('geocodable', $entity);

        return $this;
    }
}
