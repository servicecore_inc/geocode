<?php

namespace ServiceCore\Geocode;

use Doctrine\Common\EventManager as DoctrineEventManager;
use Doctrine\ORM\EntityManager;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;
use ServiceCore\Geocode\Listener\OnGeocodableUpdate;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        return require __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e): void
    {
        $application    = $e->getApplication();
        $serviceManager = $application->getServiceManager();

        /** @var EntityManager $entityManager */
        $entityManager = $serviceManager->get(EntityManager::class);

        /** @var DoctrineEventManager $doctrineEventManager */
        $doctrineEventManager = $entityManager->getEventManager();

        /** @var OnGeocodableUpdate $onGeocodableUpdate */
        $onGeocodableUpdate = $serviceManager->get(OnGeocodableUpdate::class);

        $onGeocodableUpdate->attach($doctrineEventManager);
    }
}
