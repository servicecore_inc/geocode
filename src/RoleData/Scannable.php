<?php

namespace ServiceCore\Geocode\RoleData;

interface Scannable
{
    public function scanForGeocodables(int $startAt = 0, int $limit = 30): array;

    /**
     * @param int $startAt
     * @param int $limit
     *
     * @return Geocodable[]
     */
    public function scanForReverseGeocodables(int $startAt = 0, int $limit = 30): array;
}
