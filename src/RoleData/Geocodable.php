<?php

namespace ServiceCore\Geocode\RoleData;

use Geocoder\Collection;

interface Geocodable
{
    public function getId(): int;

    public function hasId(): bool;

    public function getGeocodableAddress(): ?string;

    public function getAddressFields(): array;

    public function getLatitude(): ?float;

    public function setLatitude(?float $latitude = null);

    public function getLongitude(): ?float;

    public function setLongitude(?float $longitude = null);

    public function getAddressCollection(): ?Collection;

    public function setAddressCollection(Collection $addressCollection);
}
