<?php

namespace ServiceCore\Geocode\Command;

use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\RoleData\Scannable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Geocode extends Command
{
    private GeocodeContext $geocoder;
    private array          $repositories;

    public function __construct(GeocodeContext $geocoder, array $repositories)
    {
        parent::__construct('Geocode');

        $this->geocoder     = $geocoder;
        $this->repositories = $repositories;
    }

    protected function configure(): void
    {
        $this->setName('sc:geocode')
            ->setDescription('Geocode locations into lat/longs')
            ->setHelp(<<<EOT
The <info>%command.name%</info> command is meant to geocode sites into latitudes and longitudes.
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $totalEntities = \count($this->repositories);

        $output->writeln("Initiating geocode ({$totalEntities} total entities)");

        $i = 1;
        foreach ($this->repositories as $entity => $repository) {
            if (!$repository instanceof Scannable) {
                $output->writeln(
                    \sprintf(
                        'Repository %s does not implement %s...Skipping',
                        \get_class($repository),
                        Scannable::class
                    )
                );
            } else {
                $output->writeln("Entity {$i} of {$totalEntities}: {$entity}");

                $this->geocoder->geocode($repository);

                $output->writeln('Entity ' . $i++ . " of {$totalEntities} complete!");
            }
        }

        $output->writeln('Geocode complete!');

        return 0;
    }
}
