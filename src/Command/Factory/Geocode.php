<?php

namespace ServiceCore\Geocode\Command\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Geocode\Command\Geocode as GeocodeCommand;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\RoleData\Scannable;

class Geocode implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): GeocodeCommand
    {
        $config = $container->get('config');

        if (!\array_key_exists('geocoder', $config)) {
            throw new RuntimeException('Missing `geocoder` key from config');
        }

        if (!\array_key_exists('entities', $config['geocoder']) || !\is_array($config['geocoder']['entities'])) {
            throw new RuntimeException('Missing `entities` array from geocoder config');
        }

        /** @var GeocodeContext $geocoder */
        $geocoder = $container->get(GeocodeContext::class);

        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        $repositories = [];
        foreach ($config['geocoder']['entities'] as $entity) {
            $repository = $entityManager->getRepository($entity);

            if ($repository instanceof Scannable) {
                $repositories[$entity] = $repository;
            }
        }

        return new GeocodeCommand($geocoder, $repositories);
    }
}
