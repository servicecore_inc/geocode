<?php

namespace ServiceCore\Geocode\Listener\Factory;

use Interop\Container\ContainerInterface;
use Laminas\EventManager\EventManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Geocode\Context\Geocode;
use ServiceCore\Geocode\Listener\OnGeocodableUpdate as OnGeocodableUpdateListener;

class OnGeocodableUpdate implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): OnGeocodableUpdateListener {
    
        $config = $container->get('config');
        $emName = $config['geocoder']['event_manager']['name'];

        /** @var Geocode $geocode */
        $geocode = $container->get(Geocode::class);

        /** @var EventManager $eventManager */
        $eventManager = $container->get($emName);

        return new OnGeocodableUpdateListener($geocode, $eventManager);
    }
}
