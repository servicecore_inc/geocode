<?php

namespace ServiceCore\Geocode\Listener;

use Doctrine\Common\EventManager as DoctrineEventManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Laminas\EventManager\EventManager as GeocoderEventManager;
use ServiceCore\Geocode\Context\Geocode;
use ServiceCore\Geocode\Event\Geocoded;
use ServiceCore\Geocode\RoleData\Geocodable;

class OnGeocodableUpdate
{
    /** @var Geocode */
    private $geocode;

    /** @var GeocoderEventManager */
    private $eventManager;

    /** @var bool */
    private $hasManualCoordinates = false;

    public function __construct(Geocode $geocode, GeocoderEventManager $eventManager)
    {
        $this->geocode      = $geocode;
        $this->eventManager = $eventManager;
    }

    public function attach(DoctrineEventManager $eventManager): void
    {
        $eventManager->addEventListener(
            [Events::prePersist],
            $this
        );

        $eventManager->addEventListener(
            [Events::preUpdate],
            $this
        );
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getEntity();

        if ($entity instanceof Geocodable) {
            if ($entity->getLatitude() === null || $entity->getLongitude() === null) {
                $this->geocode->geocodeEntities([$entity]);
            }

            $this->eventManager->trigger(Geocoded::class, new Geocoded($this, $entity));
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getEntity();

        if ($entity instanceof Geocodable) {
            if ($this->addressChanged($eventArgs, $entity)) {
                $this->geocode->geocodeEntities([$entity]);

                $this->eventManager->trigger(Geocoded::class, new Geocoded($this, $entity));

                return;
            }

            if ($this->hasManualCoordinates) {
                $this->eventManager->trigger(Geocoded::class, new Geocoded($this, $entity));

                $this->hasManualCoordinates = false;

                return;
            }
        }
    }

    private function addressChanged(PreUpdateEventArgs $eventArgs, Geocodable $entity): bool
    {
        $this->hasManualCoordinates = $entity->getLatitude() !== null && $entity->getLongitude() !== null
            && ($eventArgs->hasChangedField('latitude') || $eventArgs->hasChangedField('longitude'));

        if ($this->hasManualCoordinates) {
            return false;
        }

        foreach ($entity->getAddressFields() as $field) {
            if ($eventArgs->hasChangedField($field)) {
                return true;
            }
        }

        return false;
    }
}
