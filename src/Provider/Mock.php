<?php

namespace ServiceCore\Geocode\Provider;

use Geocoder\Collection;
use Geocoder\Geocoder;
use Geocoder\Model\Address;
use Geocoder\Model\AddressCollection;
use Geocoder\Model\AdminLevelCollection;
use Geocoder\Model\Coordinates;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;

class Mock implements Geocoder
{
    /**
     * Key value pair of addresses coded to latitudes and longitudes.
     *
     * @var array|mixed
     */
    private $geocodes = [];

    public function __construct(array $options = null)
    {
        if ($options && \array_key_exists('geocodes', $options)) {
            $this->geocodes = $options['geocodes'];
        }
    }

    /**
     * Geocodes a given value.
     *
     * @param string $value
     *
     * @return Collection
     */
    public function geocode(string $value): Collection
    {
        $latitude  = 0.0;
        $longitude = 0.0;

        if (\array_key_exists($value, $this->geocodes)) {
            $latitude  = $this->geocodes[$value]['latitude'];
            $longitude = $this->geocodes[$value]['longitude'];
        }

        return new AddressCollection([
            new Address(new Coordinates($latitude, $longitude))
        ]);
    }

    /**
     * Reverses geocode given latitude and longitude values.
     *
     * @param double $latitude
     * @param double $longitude
     *
     * @return Collection
     */
    public function reverse(float $latitude, float $longitude): Collection
    {
        return new AddressCollection();
    }

    /**
     * Returns the maximum number of Address objects that can be
     * returned by `geocode()` or `reverse()` methods.
     *
     * @return integer
     */
    public function getLimit()
    {
        return 100;
    }

    /**
     * Sets the maximum number of `Address` objects that can be
     * returned by `geocode()` or `reverse()` methods.
     *
     * @param integer $limit
     *
     * @return Geocoder
     */
    public function limit($limit)
    {
        return $this;
    }

    public function geocodeQuery(GeocodeQuery $query): Collection
    {
        $latitude  = 0.0;
        $longitude = 0.0;

        if (\array_key_exists($query->getText(), $this->geocodes)) {
            $latitude  = $this->geocodes[$query->getText()]['latitude'];
            $longitude = $this->geocodes[$query->getText()]['longitude'];
        }

        return new AddressCollection(
            [
                new Address('Mock', new AdminLevelCollection(), new Coordinates($latitude, $longitude))
            ]
        );
    }

    public function reverseQuery(ReverseQuery $query): Collection
    {
        return new AddressCollection(
            [
                new Address('Mock', new AdminLevelCollection())
            ]
        );
    }

    public function getName(): string
    {
        return '';
    }
}
