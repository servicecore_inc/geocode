<?php

namespace ServiceCore\Geocode\Context;

use Geocoder\Collection;
use Geocoder\Location;
use Geocoder\Model\AddressCollection;
use Geocoder\Model\Coordinates;
use Geocoder\Provider\Provider;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Laminas\EventManager\EventManager as GeocoderEventManager;
use ServiceCore\Geocode\Event\GeocodeFailed;
use ServiceCore\Geocode\Event\PostBatch;
use ServiceCore\Geocode\Event\PreBatch;
use ServiceCore\Geocode\RoleData\Geocodable;
use ServiceCore\Geocode\RoleData\Scannable;
use Throwable;

class Geocode
{
    public const BATCH_SIZE = 30;
    public const NO_RESULT  = null;

    /** @var Provider */
    private $geocoder;

    /** @var GeocoderEventManager */
    private $eventManager;

    public function __construct(Provider $geocoder, GeocoderEventManager $eventManager)
    {
        $this->geocoder     = $geocoder;
        $this->eventManager = $eventManager;
    }

    public function geocode(Scannable $repository): void
    {
        $startId = 0;
        while ($entities = $repository->scanForGeocodables($startId, self::BATCH_SIZE)) {
            $this->eventManager->trigger(PreBatch::class, new PreBatch($startId));

            $this->geocodeEntities($entities);

            $startId = $entities[\count($entities) - 1]->getId() + 1;

            $this->eventManager->trigger(PostBatch::class, new PostBatch($entities));
        }
    }

    public function reverseGeocode(Scannable $repository): void
    {
        $startId = 0;
        while ($entities = $repository->scanForReverseGeocodables($startId, self::BATCH_SIZE)) {
            $this->eventManager->trigger(PreBatch::class, new PreBatch($startId));

            $this->reverseGeocodeEntities($entities);

            $startId = $entities[\count($entities) - 1]->getId() + 1;

            $this->eventManager->trigger(PostBatch::class, new PostBatch($entities));
        }
    }

    public function geocodeEntities(array $entities): array
    {
        $failedEntityIds = [];

        foreach ($entities as $entity) {
            if ($failedId = $this->geocodeEntity($entity)) {
                $failedEntityIds[] = $failedId;
            }
        }

        return $failedEntityIds;
    }

    public function reverseGeocodeEntities(array $entities): void
    {
        foreach ($entities as $entity) {
            $this->reverseGeocodeEntity($entity);
        }
    }

    private function geocodeEntity(Geocodable $entity): ?int
    {
        if ($geocodableAddress = $entity->getGeocodableAddress()) {
            $exception = null;

            try {
                $addresses = $this->geocoder->geocodeQuery(GeocodeQuery::create($geocodableAddress));
                $latitude  = $this->getLatitude($addresses);
                $longitude = $this->getLongitude($addresses);
            } catch (Throwable $exception) {
                $latitude  = self::NO_RESULT;
                $longitude = self::NO_RESULT;
                $addresses = new AddressCollection();
            }

            $entity->setLatitude($latitude)
                   ->setLongitude($longitude)
                   ->setAddressCollection($addresses);

            if ($exception instanceof Throwable) {
                $this->eventManager->trigger(GeocodeFailed::class, new GeocodeFailed($entity, $exception));
            }

            if ($addresses->isEmpty() && $entity->hasId()) {
                return $entity->getId();
            }
        }

        return null;
    }

    private function reverseGeocodeEntity(Geocodable $entity): void
    {
        if ($entity->getLatitude() && $entity->getLongitude()) {
            $exception = null;

            try {
                $coordinates = new Coordinates($entity->getLatitude(), $entity->getLongitude());
                $addresses   = $this->geocoder->reverseQuery(ReverseQuery::create($coordinates));
            } catch (Throwable $exception) {
                $addresses = new AddressCollection();
            }

            $entity->setAddressCollection($addresses);

            if ($exception instanceof Throwable) {
                $this->eventManager->trigger(GeocodeFailed::class, new GeocodeFailed($entity, $exception));
            }
        }
    }

    private function getLatitude(Collection $addresses): ?float
    {
        $latitude = self::NO_RESULT;

        if ($location = $this->getLocation($addresses)) {
            $latitude = $location->getCoordinates() ? $location->getCoordinates()->getLatitude() : self::NO_RESULT;
        }

        return $latitude;
    }

    private function getLongitude(Collection $addresses): ?float
    {
        $longitude = self::NO_RESULT;

        if ($location = $this->getLocation($addresses)) {
            $longitude = $location->getCoordinates() ? $location->getCoordinates()->getLongitude() : self::NO_RESULT;
        }

        return $longitude;
    }

    private function getLocation(Collection $addresses): ?Location
    {
        $location = null;

        if (!$addresses->isEmpty()) {
            $location = $addresses->first();
        }

        return $location;
    }
}
