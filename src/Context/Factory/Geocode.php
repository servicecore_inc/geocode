<?php

namespace ServiceCore\Geocode\Context\Factory;

use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Provider\Here\Here;
use Http\Adapter\Guzzle7\Client;
use Interop\Container\ContainerInterface;
use Laminas\EventManager\EventManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\Provider\Mock;

class Geocode implements FactoryInterface
{
    public const PROVIDER_NAME_GOOGLE      = 'google';
    public const PROVIDER_NAME_GOOGLE_MAPS = 'googleMaps';
    public const PROVIDER_NAME_HERE        = 'here';
    public const PROVIDER_NAME_MOCK        = 'mock';

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): GeocodeContext
    {
        $config          = $container->get('config');
        $providerName    = $config['geocoder']['provider']['name'];
        $providerOptions = $config['geocoder']['provider']['options'];

        if (!\is_array($providerOptions)) {
            throw new RuntimeException('The `options` key must be an array');
        }

        switch ($providerName) {
            case self::PROVIDER_NAME_GOOGLE:
            case self::PROVIDER_NAME_GOOGLE_MAPS:
                $this->validateProviderOptionKey($providerOptions, 'apiKey');

                $adapter  = new Client();
                $geocoder = new GoogleMaps(
                    $adapter,
                    null,
                    $providerOptions['apiKey']
                );
                break;
            case self::PROVIDER_NAME_HERE:
                $this->validateProviderOptionKey($providerOptions, 'apiKey');

                $adapter  = new Client();
                $geocoder = Here::createUsingApiKey($adapter, $providerOptions['apiKey']);
                break;
            case self::PROVIDER_NAME_MOCK:
                $geocodes = $providerOptions['geocodes'] ?? [];
                $geocoder = new Mock(['geocodes' => $geocodes]);
                break;
            default:
                throw new RuntimeException('Could not find valid geocode provider');
        }

        $emName = $config['geocoder']['event_manager']['name'] ?? null;

        if (!$emName || !\is_string($emName)) {
            throw new RuntimeException('Could not retrieve event manager name');
        }

        /** @var EventManager $eventManager */
        $eventManager = $container->get($emName);

        return new GeocodeContext($geocoder, $eventManager);
    }

    private function validateProviderOptionKey(array $providerOptions, string $key): void
    {
        if (!\array_key_exists($key, $providerOptions)) {
            throw new RuntimeException("Missing `{$key}` in provider options");
        }
    }
}
