<?php

namespace ServiceCore\Geocode;

use Laminas\Mvc\Service\EventManagerFactory;
use ServiceCore\Geocode\Command\Factory\Geocode as GeocodeCommandFactory;
use ServiceCore\Geocode\Command\Geocode as GeocodeCommand;
use ServiceCore\Geocode\Context\Factory\Geocode as GeocodeContextFactory;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\Listener\Factory\OnGeocodableUpdate as OnGeocodableUpdateFactory;
use ServiceCore\Geocode\Listener\OnGeocodableUpdate;

$eventManager = 'geocoder.eventManager.default';

return [
    'geocoder' => [
        'event_manager' => [
            'name' => $eventManager
        ],

        'provider' => [
            'name'    => null,
            'options' => []
        ]
    ],

    'service_manager' => [
        'factories' => [
            $eventManager             => EventManagerFactory::class,
            GeocodeContext::class     => GeocodeContextFactory::class,
            OnGeocodableUpdate::class => OnGeocodableUpdateFactory::class,
            GeocodeCommand::class     => GeocodeCommandFactory::class
        ]
    ],

    'laminas-cli' => [
        'commands' => [
            'sc:geocode' => GeocodeCommand::class,
        ],
    ],
];
