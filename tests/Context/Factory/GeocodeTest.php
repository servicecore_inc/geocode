<?php

namespace ServiceCore\Geocode\Test\Context\Factory;

use Doctrine\ORM\EntityManager;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Provider\Here\Here;
use Interop\Container\ContainerInterface;
use Laminas\EventManager\EventManager;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase as Test;
use ReflectionClass;
use ReflectionProperty;
use ServiceCore\Geocode\Context\Factory\Geocode as GeocodeContextFactory;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\Provider\Mock;

/**
 * @group context
 * @group context-factory
 * @TODO  Update tests in this class to not use the real ServiceManager. See `testInvokeReturnsContextWithHere`
 */
class GeocodeTest extends Test
{
    public function testInvokeReturnsContextWithGoogleMaps(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = new ServiceManager();

        $this->setContainerConfig($container, 'google');

        $context           = $factory($container, GeocodeContext::class);
        $reflectedContext  = new ReflectionClass($context);
        $reflectedProperty = $reflectedContext->getProperty('geocoder');

        $reflectedProperty->setAccessible(true);

        self::assertInstanceOf(GoogleMaps::class, $reflectedProperty->getValue($context));
    }

    public function testInvokeReturnsContextWithHere(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = $this->createMock(ContainerInterface::class);
        $apiKey    = 'fooBar';
        $emName    = 'fooBar';
        $config    = [
            'geocoder' => [
                'provider'      => [
                    'name'    => GeocodeContextFactory::PROVIDER_NAME_HERE,
                    'options' => [
                        'apiKey' => $apiKey,
                    ]
                ],
                'event_manager' => [
                    'name' => $emName
                ]
            ]
        ];

        $eventManager = $this->createPartialMock(EventManager::class, []);

        $container->expects(self::exactly(2))
                  ->method('get')
                  ->willReturnOnConsecutiveCalls($config, $eventManager);

        $context           = $factory($container, GeocodeContext::class);
        $reflectedContext  = new ReflectionClass($context);
        $reflectedProperty = $reflectedContext->getProperty('geocoder');

        $reflectedProperty->setAccessible(true);

        /** @var Here $provider */
        $provider = $reflectedProperty->getValue($context);

        self::assertInstanceOf(Here::class, $provider);

        $apiKeyProp = new ReflectionProperty($provider, 'apiKey');

        $apiKeyProp->setAccessible(true);

        self::assertEquals($apiKey, $apiKeyProp->getValue($provider));
    }

    public function testInvokeThrowsExceptionIfProviderInvalid(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = $this->createMock(ContainerInterface::class);
        $config    = [
            'geocoder' => [
                'provider' => [
                    'name'    => null,
                    'options' => []
                ],
            ]
        ];

        $container->expects(self::once())
                  ->method('get')
                  ->willReturn($config);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Could not find valid geocode provider');

        $factory($container, GeocodeContext::class);
    }

    public function testInvokeThrowsRuntimeExceptionIfOptionsNotArray(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = $this->createMock(ContainerInterface::class);
        $config    = [
            'geocoder' => [
                'provider' => [
                    'name'    => GeocodeContextFactory::PROVIDER_NAME_HERE,
                    'options' => 'fooBar'
                ],
            ]
        ];

        $container->expects(self::once())
                  ->method('get')
                  ->willReturn($config);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('The `options` key must be an array');

        $factory($container, GeocodeContext::class);
    }

    public function testInvokeThrowsExceptionIfEmNameMissing(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = $this->createMock(ContainerInterface::class);
        $config    = [
            'geocoder' => [
                'provider' => [
                    'name'    => GeocodeContextFactory::PROVIDER_NAME_HERE,
                    'options' => [
                        'apiKey' => 'fooBar'
                    ]
                ],
            ]
        ];

        $container->expects(self::once())
                  ->method('get')
                  ->willReturn($config);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Could not retrieve event manager name');

        $factory($container, GeocodeContext::class);
    }

    public function testInvokeReturnsContextWithMock(): void
    {
        $factory   = new GeocodeContextFactory();
        $container = new ServiceManager();

        $this->setContainerConfig($container, 'mock');

        $context           = $factory($container, GeocodeContext::class);
        $reflectedContext  = new ReflectionClass($context);
        $reflectedProperty = $reflectedContext->getProperty('geocoder');

        $reflectedProperty->setAccessible(true);

        self::assertInstanceOf(Mock::class, $reflectedProperty->getValue($context));
    }

    private function setContainerConfig(ServiceManager $container, string $providerName = ''): void
    {
        $evm = new EventManager();

        $container->setAllowOverride(true);

        $container->setService('default_evm', $evm);
        $container->setService(
            'config',
            [
                'geocoder' => [
                    'provider'      => [
                        'name'    => $providerName,
                        'options' => [
                            'apiKey' => 'fooBar'
                        ]
                    ],
                    'event_manager' => [
                        'name' => 'default_evm'
                    ]
                ]
            ]
        );

        $container->setService(EntityManager::class, $this->createStub(EntityManager::class));
    }
}
