<?php

namespace ServiceCore\Geocode\Test\Context;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping;
use Geocoder\Model\AddressCollection;
use Laminas\EventManager\EventManager;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\Provider\Mock as MockProvider;
use ServiceCore\Geocode\RoleData\Scannable;
use ServiceCore\Geocode\Test\Mock\Entity as MockEntity;
use ServiceCore\Geocode\Test\Mock\Repository as MockRepository;

class GeocodeTest extends Test
{
    private const GEOCODABLE_ADDRESS = '405 Urban Street, Lakewood, CO 80228';
    private const GEOCODABLE_LAT     = -39.432;
    private const GEOCODABLE_LONG    = 120.3431;

    public function testGeocode(): void
    {
        $context = $this->getContext();
        $entity  = new MockEntity(self::GEOCODABLE_ADDRESS);

        $context->geocode($this->getMockRepository($entity));

        self::assertEquals(self::GEOCODABLE_LAT, $entity->getLatitude());
        self::assertEquals(self::GEOCODABLE_LONG, $entity->getLongitude());
    }

    public function testGeocodeEntities(): void
    {
        $context = $this->getContext();
        $entity  = new MockEntity(self::GEOCODABLE_ADDRESS);

        $result = $context->geocodeEntities([$entity]);

        self::assertEquals(self::GEOCODABLE_LAT, $entity->getLatitude());
        self::assertEquals(self::GEOCODABLE_LONG, $entity->getLongitude());
        self::assertEquals([], $result);
    }

    public function testGeocodeEntitiesFails(): void
    {
        $provider = $this->createStub(MockProvider::class);

        $provider->method('geocodeQuery')->willThrowException(new \Exception());

        $eventManager = $this->createStub(EventManager::class);

        $eventManager->expects(self::once())->method('trigger');

        $context = new GeocodeContext(
            $provider,
            $eventManager
        );

        $entity = new MockEntity('fail');

        $result = $context->geocodeEntities([$entity]);

        self::assertEquals(null, $entity->getLatitude());
        self::assertEquals(null, $entity->getLongitude());
        self::assertEquals([$entity->getId()], $result);

        $provider->__phpunit_verify();
        $eventManager->__phpunit_verify();
    }

    public function testGeocodeEntitiesWithEmptyAddresses(): void
    {
        $provider     = $this->createStub(MockProvider::class);
        $eventManager = $this->createStub(EventManager::class);

        $provider->method('geocodeQuery')->willReturn(new AddressCollection());

        $context    = new GeocodeContext($provider, $eventManager);
        $mockEntity = new MockEntity('1111 Test St');

        $result = $context->geocodeEntities([$mockEntity]);

        self::assertNull($mockEntity->getLatitude());
        self::assertNull($mockEntity->getLongitude());
        self::assertEquals([$mockEntity->getId()], $result);
    }

    public function testReverseGeocode(): void
    {
        $context = $this->getContext();
        $entity  = new MockEntity(self::GEOCODABLE_ADDRESS);

        $entity->setLatitude(self::GEOCODABLE_LAT)
               ->setLongitude(self::GEOCODABLE_LONG)
               ->setExpectedAddress(null);

        $context->reverseGeocode($this->getMockRepository($entity));

        self::assertNotEmpty($entity->getAddressCollection());
    }

    public function testReverseGeocodeEntities(): void
    {
        $context = $this->getContext();
        $entity  = new MockEntity(self::GEOCODABLE_ADDRESS);

        $entity->setLatitude(self::GEOCODABLE_LAT)
               ->setLongitude(self::GEOCODABLE_LONG)
               ->setExpectedAddress(null);

        $context->reverseGeocodeEntities([$entity]);

        self::assertNotEmpty($entity->getAddressCollection());
    }

    private function getMockRepository(MockEntity $mockEntity): Scannable
    {
        $entityManager = $this->createStub(EntityManager::class);
        $classMetadata = new Mapping\ClassMetadata('GeocodeEntity');

        return new MockRepository($entityManager, $classMetadata, $mockEntity);
    }

    private function getContext(?EventManager $eventManager = null): GeocodeContext
    {
        return new GeocodeContext(
            new MockProvider(
                [
                    'geocodes' => [
                        self::GEOCODABLE_ADDRESS => [
                            'latitude'  => self::GEOCODABLE_LAT,
                            'longitude' => self::GEOCODABLE_LONG
                        ]
                    ]
                ]
            ),
            $eventManager ?: new EventManager()
        );
    }
}
