<?php

namespace ServiceCore\Geocode\Test\Mock;

use Geocoder\Collection;
use Geocoder\Model\AddressCollection;
use ServiceCore\Geocode\RoleData\Geocodable;

/**
 * @author Andy O'Brien <andy@servicecore.com>
 */
class Entity implements Geocodable
{
    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /** @var string|null */
    private $expectedAddress;

    /** @var Collection */
    private $addressCollection;

    /**
     * @param string $expectedAddress
     */
    public function __construct(string $expectedAddress)
    {
        $this->expectedAddress   = $expectedAddress;
        $this->addressCollection = new AddressCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return 1;
    }

    public function hasId(): bool
    {
        return true;
    }

    /**
     * @return null|string
     */
    public function getGeocodableAddress(): ?string
    {
        return $this->expectedAddress;
    }

    /**
     * @return array
     */
    public function getAddressFields(): array
    {
        return ['address', 'city', 'state', 'zip',];
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     *
     * @return $this
     */
    public function setLatitude(?float $latitude = null): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     *
     * @return $this
     */
    public function setLongitude(?float $longitude = null): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAddressCollection(): ?Collection
    {
        return $this->addressCollection;
    }

    /**
     * @param Collection $addressCollection
     *
     * @return $this
     */
    public function setAddressCollection(Collection $addressCollection): self
    {
        $this->addressCollection = $addressCollection;

        return $this;
    }

    /**
     * @param string|null $address
     *
     * @return Entity
     */
    public function setExpectedAddress(?string $address): self
    {
        $this->expectedAddress = $address;

        return $this;
    }
}
