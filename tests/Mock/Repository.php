<?php

namespace ServiceCore\Geocode\Test\Mock;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use ServiceCore\Geocode\RoleData\Geocodable;
use ServiceCore\Geocode\RoleData\Scannable;

class Repository extends EntityRepository implements Scannable
{
    /** @var Geocodable */
    private $geocodableEntity;

    /**
     * @param EntityManager $em
     * @param ClassMetadata $class
     * @param Geocodable    $geocodable
     */
    public function __construct(EntityManager $em, ClassMetadata $class, Geocodable $geocodable)
    {
        parent::__construct($em, $class);

        $this->geocodableEntity = $geocodable;
    }

    /**
     * @param int $startAt
     * @param int $limit
     *
     * @return array
     */
    public function scanForGeocodables(int $startAt = 0, int $limit = 30): array
    {
        $geocodeable = [];

        // Only run 1 iteration
        if ($startAt === 0) {
            $geocodeable = [
                $this->geocodableEntity
            ];
        }

        return $geocodeable;
    }

    /**
     * @param int $startAt
     * @param int $limit
     *
     * @return array
     */
    public function scanForReverseGeocodables(int $startAt = 0, int $limit = 30): array
    {
        $reverseable = [];

        // Only run 1 iteration
        if ($startAt === 0) {
            $reverseable = [
                $this->geocodableEntity
            ];
        }

        return $reverseable;
    }
}
