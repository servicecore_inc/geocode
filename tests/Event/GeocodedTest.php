<?php

namespace ServiceCore\Geocode\Test\Event;

use ServiceCore\Geocode\Event\Geocoded;
use ServiceCore\Geocode\Test\Mock\Entity as MockEntity;
use PHPUnit\Framework\TestCase as Test;
use ReflectionClass;

/**
 * @group event
 */
class GeocodedTest extends Test
{
    public function testConstructor(): void
    {
        $target         = new class {};
        $mockEntity     = new MockEntity('');
        $event          = new Geocoded($target, $mockEntity);
        $reflectedEvent = new ReflectionClass($event);

        $reflectedProperty = $reflectedEvent->getProperty('name');

        $reflectedProperty->setAccessible(true);

        self::assertEquals(Geocoded::class, $reflectedProperty->getValue($event));

        $reflectedProperty = $reflectedEvent->getProperty('target');

        $reflectedProperty->setAccessible(true);

        self::assertSame($target, $reflectedProperty->getValue($event));

        $reflectedProperty = $reflectedEvent->getProperty('params');

        $reflectedProperty->setAccessible(true);

        self::assertArrayHasKey('geocodable', $reflectedProperty->getValue($event));
        self::assertSame($mockEntity, $reflectedProperty->getValue($event)['geocodable']);
    }

    public function testGetGeocodable(): void
    {
        $mockEntity = new MockEntity('');
        $event      = new Geocoded(new class {}, $mockEntity);

        self::assertSame($mockEntity, $event->getGeocodable());
    }

    public function testSetGeocodable(): void
    {
        $event      = new Geocoded(new class {}, new MockEntity(''));
        $mockEntity = new MockEntity('abc');

        self::assertSame($event, $event->setGeocodable($mockEntity));

        $reflectedEvent = new ReflectionClass($event);

        $reflectedProperty = $reflectedEvent->getProperty('params');

        $reflectedProperty->setAccessible(true);

        self::assertSame($mockEntity, $reflectedProperty->getValue($event)['geocodable']);
    }
}
