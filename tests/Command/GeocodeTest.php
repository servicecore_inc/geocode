<?php

namespace ServiceCore\Geocode\Test\Command;

use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use ServiceCore\Geocode\Command\Geocode;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\RoleData\Scannable;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeocodeTest extends TestCase
{
    public function testRun(): void
    {
        $geocoder = $this->createMock(GeocodeContext::class);

        $geocoder->expects(self::once())->method('geocode');

        $repos = [
            $this->getMockForAbstractClass(Scannable::class)
        ];

        $command = new Geocode($geocoder, $repos);
        $input   = $this->getMockForAbstractClass(InputInterface::class);
        $output  = $this->getMockForAbstractClass(
            OutputInterface::class,
            [],
            '',
            true,
            true,
            true,
            [
                'writeln'
            ]
        );

        $output->expects(self::exactly(4))->method('writeln');

        $this->assertEquals(0, $command->run($input, $output));
    }

    public function testRunIfRepositoryNotInstanceOfScannable(): void
    {
        $geocoder = $this->createMock(GeocodeContext::class);

        $geocoder->expects(self::never())->method('geocode');

        $repos = [
            $this->createMock(EntityRepository::class)
        ];

        $command = new Geocode($geocoder, $repos);
        $input   = $this->getMockForAbstractClass(InputInterface::class);
        $output  = $this->getMockForAbstractClass(
            OutputInterface::class,
            [],
            '',
            true,
            true,
            true,
            [
                'writeln'
            ]
        );

        $output->expects(self::exactly(3))->method('writeln');

        $command->run($input, $output);
    }
}
