<?php

namespace ServiceCore\Geocode\Test\Listener;

use Doctrine\Common\EventManager as DoctrineEventManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Laminas\EventManager\EventManager;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Geocode\Context\Geocode as GeocodeContext;
use ServiceCore\Geocode\Listener\OnGeocodableUpdate as Listener;
use ServiceCore\Geocode\Test\Mock\Entity;

/**
 * @group listener
 */
class OnGeocodableUpdateTest extends Test
{
    public function testAttach(): void
    {
        $geocode      = $this->createStub(GeocodeContext::class);
        $eventManager = $this->createStub(EventManager::class);
        $listener     = new Listener($geocode, $eventManager);
        $eventManager = $this->createStub(DoctrineEventManager::class);

        $eventManager->expects(self::exactly(2))
                     ->method('addEventListener');

        $listener->attach($eventManager);
    }

    public function testPrePersist(): void
    {
        $geocode = $this->createStub(GeocodeContext::class);

        $geocode->expects(self::once())
                ->method('geocodeEntities');

        $eventManager = $this->createStub(EventManager::class);

        $eventManager->expects(self::once())
                     ->method('trigger');

        $listener  = new Listener($geocode, $eventManager);
        $eventArgs = $this->createStub(LifecycleEventArgs::class);

        $eventArgs->expects(self::once())
                  ->method('getEntity')
                  ->willReturn(new Entity(''));

        $listener->prePersist($eventArgs);
    }

    public function testPrePersistWith0LatitudeAndLongitude(): void
    {
        $geocode = $this->createStub(GeocodeContext::class);

        $geocode->expects(self::never())
                ->method('geocodeEntities');

        $eventManager = $this->createStub(EventManager::class);

        $eventManager->expects(self::once())
                     ->method('trigger');

        $listener  = new Listener($geocode, $eventManager);
        $eventArgs = $this->createStub(LifecycleEventArgs::class);
        $entity    = new Entity('');

        $entity->setLatitude(0)
               ->setLongitude(0);

        $eventArgs->expects(self::once())
                  ->method('getEntity')
                  ->willReturn($entity);

        $listener->prePersist($eventArgs);
    }

    public function testPreUpdate(): void
    {
        $geocode = $this->createStub(GeocodeContext::class);

        $geocode->expects(self::once())
                ->method('geocodeEntities');

        $eventManager = $this->createStub(EventManager::class);

        $eventManager->expects(self::once())
                     ->method('trigger');

        $listener  = new Listener($geocode, $eventManager);
        $eventArgs = $this->createStub(PreUpdateEventArgs::class);

        $eventArgs->expects(self::once())
                  ->method('getEntity')
                  ->willReturn(new Entity(''));

        $eventArgs->expects(self::once())
                  ->method('hasChangedField')
                  ->willReturn(true);

        $listener->preUpdate($eventArgs);
    }

    public function testPreUpdateWithManualCoordinates(): void
    {
        $geocode = $this->createStub(GeocodeContext::class);

        $geocode->expects(self::never())
                ->method('geocodeEntities');

        $eventManager = $this->createStub(EventManager::class);

        $eventManager->expects(self::once())
                     ->method('trigger');

        $listener  = new Listener($geocode, $eventManager);
        $eventArgs = $this->createStub(PreUpdateEventArgs::class);
        $entity    = new Entity('111 Test St');

        $entity->setLatitude(0)
               ->setLongitude(0);

        $eventArgs->expects(self::once())
                  ->method('getEntity')
                  ->willReturn($entity);

        $eventArgs->expects(self::once())
                  ->method('hasChangedField')
                  ->willReturn(true);

        $listener->preUpdate($eventArgs);
    }
}
