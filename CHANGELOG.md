# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 8.0.0
### Added
- Add hasId() method to Geocodable interface

### Fixed
- Check for ID on failed Geocodable to avoid getId() failure on new (not flushed) entities
- Remove references to specific Geocodable entity type (site)

## 7.1.0
### Updated
- Return IDs of Sites that fail to geocode due to no address match
